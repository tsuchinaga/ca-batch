package usecase

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/service"
	"log"
)

type Confirm interface {
	Confirm() error
}

func NewConfirm(informationService service.Information, storeService service.Store) Confirm {
	return &confirm{
		information: informationService,
		store:       storeService,
	}
}

type confirm struct {
	information service.Information
	store       service.Store
}

func (u *confirm) Confirm() error {
	newInformation, err := u.information.Get()
	if err != nil {
		// TODO errorのラッピング
		// TODO 通知？
		return err
	}

	oldInformation := u.store.Get()

	if !u.information.Equal(oldInformation, newInformation) {
		// TODO 通知
		log.Println("不一致")
	}

	u.store.Set(newInformation)

	return nil
}
