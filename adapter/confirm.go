package adapter

import (
	"gitlab.com/tsuchinaga/ca-batch/adapter/in"
	"gitlab.com/tsuchinaga/ca-batch/adapter/out"
	"gitlab.com/tsuchinaga/ca-batch/usecase"
)

type Confirm interface {
	Confirm(in *in.ConfirmIn) *out.ConfirmOut
}

func NewConfirm(confirmUseCase usecase.Confirm) Confirm {
	return &confirm{confirmUseCase: confirmUseCase}
}

type confirm struct {
	confirmUseCase usecase.Confirm
}

func (a *confirm) Confirm(_ *in.ConfirmIn) *out.ConfirmOut {
	err := a.confirmUseCase.Confirm()
	if err != nil {
		// エラーハンドリング
	}
	return nil
}
