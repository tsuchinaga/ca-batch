package model

import "time"

type Information struct {
	CheckedAt time.Time
	Body      string
}
