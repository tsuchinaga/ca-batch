package repository

import "gitlab.com/tsuchinaga/ca-batch/domain/model"

type Information interface {
	Get() (*model.Information, error)
}
