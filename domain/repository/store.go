package repository

import "gitlab.com/tsuchinaga/ca-batch/domain/model"

type Store interface {
	Find() *model.Information
	Save(information *model.Information)
}
