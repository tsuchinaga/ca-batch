package service

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"gitlab.com/tsuchinaga/ca-batch/domain/repository"
)

type Store interface {
	Get() *model.Information
	Set(information *model.Information)
}

func NewStore(storeRepository repository.Store) Store {
	return &store{store: storeRepository}
}

type store struct {
	store repository.Store
}

func (s *store) Get() *model.Information {
	return s.store.Find()
}

func (s *store) Set(information *model.Information) {
	s.store.Save(information)
}
