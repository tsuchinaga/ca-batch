package service

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"gitlab.com/tsuchinaga/ca-batch/domain/repository"
	"reflect"
	"strconv"
	"testing"
	"time"
)

type testStoreRepository struct {
	repository.Store
	returnFind       *model.Information
	savedInformation *model.Information
}

func (r *testStoreRepository) Find() *model.Information {
	return r.returnFind
}

func (r *testStoreRepository) Save(information *model.Information) {
	r.savedInformation = information
}

func TestStore_Get(t *testing.T) {
	testTable := []struct {
		returnFind *model.Information
		expect     *model.Information
	}{
		{nil, nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: ""}, &model.Information{CheckedAt: time.Unix(1, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(2, 0), Body: ""}, &model.Information{CheckedAt: time.Unix(2, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, &model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}},
		{&model.Information{}, &model.Information{}},
	}

	for i, test := range testTable {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			service := &store{&testStoreRepository{returnFind: test.returnFind}}
			actual := service.Get()
			if !reflect.DeepEqual(test.expect, actual) {
				t.Fatalf("%s 失敗\n期待: %+v\n実際: %+v\n", t.Name(), test.expect, actual)
			}
		})
	}
}

func TestStore_Set(t *testing.T) {
	testTable := []struct {
		saveInformation *model.Information
		expect          *model.Information
	}{
		{nil, nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: ""}, &model.Information{CheckedAt: time.Unix(1, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(2, 0), Body: ""}, &model.Information{CheckedAt: time.Unix(2, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, &model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}},
		{&model.Information{}, &model.Information{}},
	}

	for i, test := range testTable {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			r := &testStoreRepository{savedInformation: nil}
			service := &store{r}
			service.Set(test.saveInformation)
			if !reflect.DeepEqual(test.expect, r.savedInformation) {
				t.Fatalf("%s 失敗\n期待: %+v\n実際: %+v\n", t.Name(), test.expect, r.savedInformation)
			}
		})
	}
}
