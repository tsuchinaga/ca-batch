package service

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"gitlab.com/tsuchinaga/ca-batch/domain/repository"
)

type Information interface {
	Get() (*model.Information, error)
	Equal(old, new *model.Information) bool
}

func NewInformation(informationRepository repository.Information) Information {
	return &information{informationRepository: informationRepository}
}

type information struct {
	informationRepository repository.Information
}

func (s *information) Get() (*model.Information, error) {
	return s.informationRepository.Get()
}

func (s *information) Equal(old, new *model.Information) bool {
	if old == nil || new == nil {
		return false
	}
	return old.Body == new.Body
}
