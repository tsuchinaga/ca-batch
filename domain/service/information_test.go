package service

import (
	"errors"
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"gitlab.com/tsuchinaga/ca-batch/domain/repository"
	"reflect"
	"strconv"
	"testing"
	"time"
)

type testInformationRepository struct {
	repository.Information
	returnGetInformation struct {
		information *model.Information
		err         error
	}
}

func (r *testInformationRepository) Get() (*model.Information, error) {
	return r.returnGetInformation.information, r.returnGetInformation.err
}

func TestInformation_Compare(t *testing.T) {
	testTable := []struct {
		old, new *model.Information
		expect   bool
	}{
		{nil, nil, false},
		{&model.Information{}, nil, false},
		{nil, &model.Information{}, false},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, &model.Information{CheckedAt: time.Unix(1, 1), Body: "Foo"}, true},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, &model.Information{CheckedAt: time.Unix(1, 0), Body: "Bar"}, false},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, &model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, true},
		{&model.Information{}, &model.Information{}, true},
	}

	for i, test := range testTable {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			service := &information{}
			actual := service.Equal(test.old, test.new)
			if test.expect != actual {
				t.Fatalf("%s 失敗\n期待: %v, 実際: %v\n", t.Name(), test.expect, actual)
			}
		})
	}
}

func TestInformation_Get(t *testing.T) {
	testTable := []struct {
		returnInformation *model.Information
		returnError       error
		expectInformation *model.Information
		expectError       error
	}{
		{nil, nil, nil, nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: ""}, nil, &model.Information{CheckedAt: time.Unix(1, 0), Body: ""}, nil},
		{&model.Information{CheckedAt: time.Unix(2, 0), Body: ""}, nil, &model.Information{CheckedAt: time.Unix(2, 0), Body: ""}, nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, nil, &model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}, nil},
		{&model.Information{}, nil, &model.Information{}, nil},
		{nil, errors.New("foo"), nil, errors.New("foo")},
	}

	for i, test := range testTable {
		test := test
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			service := information{informationRepository: &testInformationRepository{
				returnGetInformation: struct {
					information *model.Information
					err         error
				}{test.returnInformation, test.returnError},
			}}
			actualInformation, actualError := service.Get()

			if !reflect.DeepEqual(test.expectInformation, actualInformation) || !reflect.DeepEqual(test.expectError, actualError) {
				t.Fatalf("%s 失敗\n期待: %+v, %v\n実際: %+v, %v", t.Name(), test.expectInformation, test.expectError, actualInformation, actualError)
			}
		})
	}
}
