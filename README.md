# Clean Architecture Batch

バッチ処理をするシステムをClean Architectureの思想に則って開発してみる

## 構造

```
root
|-- domain : 1層目 Enterprice Business Rules
|   |-- model : データオブジェクト
|   |-- repository : データオブジェクトを取ったり入れたりの抽象的な定義
|   |-- service : modelやrepositoryを使ってビジネスロジックを実現する処理
|
|-- usecase : 2層目 Application Business Rules
|
|-- adapter : 3層目 Interface Adapters
|   |-- in : adapterへの入力
|   |-- out : adapterからの出力
|
|-- infra : 4層目 Frameworks & Drivers
```
