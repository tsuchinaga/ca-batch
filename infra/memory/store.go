package memory

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"sync"
)

var savedInformation = new(model.Information)
var mutex sync.Mutex

type Store struct{}

func (i *Store) Find() *model.Information {
	mutex.Lock()
	defer mutex.Unlock()
	return savedInformation
}

func (i *Store) Save(information *model.Information) {
	mutex.Lock()
	defer mutex.Unlock()
	savedInformation = information
}
