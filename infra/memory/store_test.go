package memory

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"testing"
	"time"
)

func TestStore_Find(t *testing.T) {
	store := new(Store)
	testTable := []struct {
		savedInformation *model.Information
	}{
		{nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(2, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}},
		{&model.Information{}},
	}

	for i, test := range testTable {
		test := test
		savedInformation = test.savedInformation
		actual := store.Find()
		if test.savedInformation != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.savedInformation, actual)
		}
	}
}

func TestStore_Save(t *testing.T) {
	store := new(Store)

	testTable := []struct {
		information *model.Information
	}{
		{nil},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(2, 0), Body: ""}},
		{&model.Information{CheckedAt: time.Unix(1, 0), Body: "Foo"}},
		{&model.Information{}},
	}

	for i, test := range testTable {
		test := test
		savedInformation = nil
		store.Save(test.information)
		if test.information != savedInformation {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.information, savedInformation)
		}
	}
}
