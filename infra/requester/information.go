package requester

import (
	"gitlab.com/tsuchinaga/ca-batch/domain/model"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type Information struct{}

func (r *Information) Get() (*model.Information, error) {
	resp, err := http.Get("https://kabu.com/process/sysmainteUpdate.js")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	byteArray, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	log.Println(string(byteArray))

	return &model.Information{
		CheckedAt: time.Now(),
		Body:      string(byteArray),
	}, nil
}
