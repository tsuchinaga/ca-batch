package requester

import (
	"testing"
)

func TestInformation_Get(t *testing.T) {
	infra := new(Information)
	_, err := infra.Get()
	if err != nil {
		t.Fatalf("%s 失敗\nエラー: %s", t.Name(), err.Error())
	}
}
