package scheduler

import "time"

func NewScheduler(interval int) *Scheduler {
	return &Scheduler{
		Interval: interval,
		jobs:     []job{new(confirmJob)},
	}
}

type Scheduler struct {
	Interval int
	jobs     []job
}

func (s *Scheduler) Run() error {
	for {
		for _, j := range s.jobs {
			if err := j.Execute(); err != nil {
				return err
			}
		}

		time.Sleep(time.Duration(s.Interval) * time.Second)
	}
}
