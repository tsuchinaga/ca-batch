package scheduler

type job interface {
	Execute() error
}
