package scheduler

import (
	"gitlab.com/tsuchinaga/ca-batch/adapter"
	"gitlab.com/tsuchinaga/ca-batch/domain/service"
	"gitlab.com/tsuchinaga/ca-batch/infra/memory"
	"gitlab.com/tsuchinaga/ca-batch/infra/requester"
	"gitlab.com/tsuchinaga/ca-batch/usecase"
)

type confirmJob struct{}

func (j *confirmJob) Execute() error {
	adapter.NewConfirm(
		usecase.NewConfirm(
			service.NewInformation(new(requester.Information)),
			service.NewStore(new(memory.Store)),
		),
	).Confirm(nil)

	return nil
}
